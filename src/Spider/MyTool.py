#!C:\Python27
# -*- coding: utf-8 -*- 

import chardet
import cookielib
import re
import requests
import sys
import sqlite3
import time
import socket
import random
import socks

import logging
logging.basicConfig(level=logging.DEBUG)

class MyTools:
    Mycookieslin = r'C:/Users/fight/AppData/Local/Google/Chrome/User Data/Default/Cookies'
    userAgentPath = r'../UserAgent.txt'
    proxyPath = r'../Proxy.txt'
#     Mycookieslin = r'C:/Users/heros/AppData/Local/Google/Chrome/User Data/Default/Cookies'
    WBCLIENT = 'ssologin.js(v1.4.13)'
    #replace the space in html
    BgnCharToSpace = re.compile('&nbsp;|&amp;')
    userAgentLists = []
    proxyLists = []

    #Find the urls
    #UrlsToNoneRex = re.compile('class="result".*?\/li')
    MyUrlsRex = re.compile('href="(.*?)"')
    CharToNoneRex = re.compile('"|href=|\'')
    ArrowToNoneRex = re.compile('>|<')
    QuoteToNoneRex = re.compile('\".*?\"')
    IpGroupToNoneRex = re.compile('eval\(unescape\(.*?</td></tr>')
    ItemsToNoneRex = re.compile('>[^<]+?<')
    NewsToNoneRex = re.compile(u'(<a href[^>]*?>)新闻')
    TitleToNoneRex = re.compile(r'>(.*?)</a')
    
    #replace all the <>
    EndCharToNoneRex = re.compile('<.*?>')

    def __init__(self):
        self.ReadHeaders()
        #self.ReadProxy()
        self.session = requests.Session()
        self.mycookies = []
        self.timeinterval_l = 5
        self.timeinterval_r = 10
#         self.session.headers['User-Agent'] = self.userAgentLists[0]
        
    def BgnReplace(self, x):
        x = self.BgnCharToSpace.sub(' ', x)
        #x = self.EndCharToNoneRex.sub('', x)
        return x
    
    def Set_Header(self, name, content):
        self.session.headers[name] = content
    
    def ReadPage(self, myurl):
        triedtime = 1
        while True:
            try:
#                 print self.session.headers
                time.sleep(random.randrange(self.timeinterval_l, self.timeinterval_r))
                myPage = self.session.get(myurl, timeout=8)
                break
            except (requests.exceptions.ConnectionError, socket.error):
                if triedtime % 5 == 4:
                    print 'connect failed, please check your connection of the net'
                    entertag = raw_input('Please enter: 1->try again, others->exit\n')
                    if entertag != '1':
                        sys.exit()
                else:
                    print 'connect failed, try to connect: %d time' % triedtime
            except (requests.exceptions.Timeout, socket.timeout):
                print 'connect timeout, please check your connection of the net'
               
            triedtime += 1
            time.sleep(3)
                    
        return myPage
    
    def ReadPageContent(self, myurl):
        myPage = self.ReadPage(myurl)
            
        try:
            myContent = myPage.content.decode('utf-8')
            #print 'encoding = utf8'
        except:
            mychar = chardet.detect(myPage.content)
            myContent = myPage.content.decode(mychar['encoding'],'ignore')
            #print 'encoding = ' + mychar['encoding']
        
        return myContent
    
    def SetCookies(self):
        self.mycookies = cookielib.LWPCookieJar()
        self.session.cookies = self.mycookies
    
    def ReadCookiesChrome(self):
        self.mycookies = cookielib.LWPCookieJar()
        
        con = sqlite3.connect(self.Mycookieslin)
        con.text_factory = str
        cur = con.cursor()
        cur.execute('SELECT host_key, path, secure, expires_utc, name, value FROM cookies')
        for items in cur.fetchall():
            c = cookielib.Cookie(0, items[4], items[5],
                     None, False,
                     items[0], items[0].startswith('.'), items[0].startswith('.'),
                     items[1], False,
                     items[2],
                     items[3],
                     items[3] == '',
                     None, None, {})
            self.mycookies.set_cookie(c)
        self.session.cookies = self.mycookies

    def ReadHeaders(self):
        fUserAgent = open(self.userAgentPath, 'r')
        self.userAgentLists = fUserAgent.readlines()
        for i in range(len(self.userAgentLists)):
            self.userAgentLists[i] = self.userAgentLists[i].replace('\n', '')
        fUserAgent.close()
    
    def ReadProxy(self):
        fProxy = open(self.proxyPath, 'r')
        self.proxyLists = fProxy.readlines()
        for i in range(len(self.proxyLists)):
            self.proxyLists[i] = self.proxyLists[i].replace('\n', '')
        fProxy.close()
        
    def SetRandProxy(self):
        rand_num = random.randrange(len(self.proxyLists))
        myProxy = self.proxyLists[rand_num].split(':')
#         self.session.proxies['http'] = 'sock5://' + self.proxyLists[rand_num]
#         self.session.proxies['https'] = 'sock5://' + self.proxyLists[rand_num]
        socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, myProxy[0], int(myProxy[1]), False, 'proxy', 'noah2012')
        socket.socket = socks.socksocket
        
    def SetRandUserAgent(self):
        rand_num = random.randrange(len(self.userAgentLists))
#         print self.userAgentLists[rand_num]
        self.session.headers['User-Agent'] = self.userAgentLists[rand_num]
    
    def SetTimeInterval(self, timeInterval):
        intervals = timeInterval.split('-')
        try:
            self.timeinterval_l = int(intervals[0])
            self.timeinterval_r = int(intervals[1])
        except:
            self.ErrorsList(5)
            sys.exit()
    
    def myPost(self, myurl, data):
        self.session.post(myurl, data=data)
            
    def ErrorsList(self, error_num):
        if error_num == '3':
            time.sleep(5)
            self.SetRandProxy()
        
        error_list = {
            '0':'Internet Connection Error:Please check your environment of Internet',
            '1':'Captcha Error:Please change the way of login (login with cookies would be better)',
            '2':'Sorry, There is No result in this page...',
            '3':'Check current Page failed:Please check the network(Maybe it\'s the problem of captcha)',
            '4':'Key Value Error: Please try again',
            '5':'Format Error: Please make sure that your interval\'s format is right',
            '11':'Cookies Path Error:Please Check the path of cookies in Chrome is right',
            '12':'Sorry login with cookies failed, please try again',
            '21':'Sorry, There is only one page result for this keyword',
            '22':'Sorry, There is no more pages',
            '23':'Sorry, There is no result for this keyword',
            '31':'Unknow Erorr with re.compile in weibo spider'
        }
        print error_list[str(error_num)]
    
if __name__ == '__main__':
#     url1 = 'http://www.baidu.com'
    url2 = 'http://weibo.com/'
    test = MyTools()
    test.ReadCookiesChrome()
    myPage1 = test.ReadPage(url2)
    print myPage1.content
#     mypage2 = test.ReadPage(url2)
    