#!C:\Python27
# -*- coding: utf-8 -*- 

import MyTool
import re
import os
import threading
import ConfigParser

import logging
logging.basicConfig(level=logging.DEBUG)

class myHousePrices:
    #rootUrl = 'http://fangjia.leju.com/'
    rootUrl = 'http://www.fangjiadp.com/'
        
    def __init__(self):
        self.mytool = MyTool.MyTools()
        
    def GetTips(self, myurl):
        self.mytool.SetRandUserAgent()
        myContent = self.mytool.ReadPageContent(myurl)
        myRegionTips = re.findall('{"PK".*?\"}', myContent, re.S)
    
        myNameList = []
        for Region in myRegionTips:
            Region = Region.replace('null', '\'null\'')
            myNameList.append(eval(Region))
            
        return myNameList
    
    def GetAllTips(self, cityName):
        #Get all the building tips of the city
        myurl = self.rootUrl + cityName     
        myTipList = self.GetTips(myurl)

        #get all regions' url
        myRegionsUrl = []
        for Tip in myTipList:
            if (Tip['RegionUrl'] not in myRegionsUrl):
                myRegionsUrl.append(Tip['RegionUrl'])
                
        myTipList = []
        myThreadList = []
        for myRegionUrl in myRegionsUrl:
            myThread = GetRegionTips(myRegionUrl, cityName)
            myThreadList.append(myThread)
            myThread.start()
            
        for i in range(len(myThreadList)):
            myThreadList[i].join()
        
        myTipList = GetRegionTips('None', cityName).myTipList[cityName]
        
        return myTipList
    
    def SetDefaultConfigure(self, path=''):
        cf = ConfigParser.ConfigParser()
        
        if len(cf.sections()) == 0:
            cf.add_section('housePrice')
        cf.set('housePrice', 'startName', '')
        
        cf.write(open(path + 'housePrice.ini', 'w'))
    
    def SetLogConfigure(self, logName, path):
        cf = ConfigParser.ConfigParser()
        cf.read(path + 'housePrice.ini')
        cf.set('housePrice', 'startName', logName)
        cf.write(open(path + 'housePrice.ini', 'w'))
        
    def WriteDownIndex(self, myTips, dataPath):
        if not os.path.exists(dataPath):
            os.mkdir(dataPath)
        
        myFile = open(dataPath + '/index.txt', 'wb')
        myFileDetail = open(dataPath + '/indexDetail.txt', 'wb')
        for tip in myTips:
            myFile.write('{\'ID\':' + str(tip['ID']) + ', \'Tip\':' + tip['Tip'].decode('utf8').encode('gbk') + '}\n')
            myFileDetail.write(str(tip) + '\n')
        myFile.close()
        myFileDetail.close()
    
    def FinishedTip(self, dataPath):
        myFinishedTip = []
        if os.path.exists(dataPath):
            for listdir in os.listdir(dataPath):
                listdir = listdir.replace('.txt', '')
                myFinishedTip.append(listdir.split(' ')[0])
            
        return myFinishedTip
    

class GetRegionTips(threading.Thread):
    rootUrl = 'http://www.fangjiadp.com/'
    #rootUrl = 'http://fangjia.leju.com/'
    myTipList = {}
    mylock = threading.RLock()
    
    def __init__(self, myRegionUrl, cityName, dataPath='../PriceData'):
        super(GetRegionTips, self).__init__()
        self.dataPath = dataPath
        self.mytool = MyTool.MyTools()
        self.myRegionUrl = myRegionUrl
        self.cityName = cityName
        
    def run(self):
        self.GetMyTips()
        
    def GetMyTips(self):
        myurl = self.rootUrl + self.myRegionUrl
        tips = self.GetTips(myurl)
        
        myDistrictsUrl = []
        for myTip in tips:
            tp = re.findall('/([\d]+)', myTip['Url'], re.S)[0]
            myTpUrl = myTip['Url'].replace(tp, '')
            myTpUrl = myTpUrl.replace('esf/', '')
            if myTpUrl not in myDistrictsUrl:
                myDistrictsUrl.append(myTpUrl)
                
        for myDistrictUrl in myDistrictsUrl:
            myurl = self.rootUrl + myDistrictUrl
            for tip in self.GetTips(myurl):
                if tip['ID'] not in tips:
                    tips.append(tip)
        
        #combine all the tips
        self.mylock.acquire()
        for tip in tips:
            try:
                if len(self.myTipList[self.cityName]) == 0:
                    self.myTipList[self.cityName] = []
            except KeyError:
                self.myTipList[self.cityName] = []
            if tip not in self.myTipList[self.cityName]:
                self.myTipList[self.cityName].append(tip)
        self.mylock.release()
        
    def GetTips(self, myurl):
        self.mytool.SetRandUserAgent()
        myContent = self.mytool.ReadPageContent(myurl)
        myRegionTips = re.findall('{"PK".*?\"}', myContent, re.S)
    
        myNameList = []
        for Region in myRegionTips:
            Region = Region.replace('null', '\'null\'')
            myNameList.append(eval(Region))
            
        return myNameList
        
if __name__ == '__main__':
    myCityName = 'shenzhen'
    dataPath = '../PriceData_' + myCityName
    HousePrice = myHousePrices()
#     HousePrice.SetDefaultConfigure('../')
#     HousePrice.SetLogConfigure('logName', '../')
 
    myTips = HousePrice.GetAllTips(myCityName)
    HousePrice.WriteDownIndex(myTips, dataPath)
    print len(myTips)
