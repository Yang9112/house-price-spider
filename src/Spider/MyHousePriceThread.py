#!C:\Python27
# -*- coding: utf-8 -*- 

import MyHousePrice
import threading
import re
import os
import time
import MyTool
# import chardet

class HPthread(threading.Thread):
    rootUrl = 'http://fangjia.leju.com/'
    mylock = threading.RLock()
    
    def __init__(self, myTip, dataPath='../PriceData'):
        super(HPthread, self).__init__()
        self.myTip = myTip
        self.dataPath = dataPath
        self.mytool = MyTool.MyTools()
        
    def run(self):
        [myRoomSize, myRoomType] = self.GetRoomSize(self.rootUrl + self.myTip['Url'])
        self.WriteDownRooms(myRoomSize, myRoomType, self.myTip, self.dataPath)
    
    def GetRoomSize(self, myurl):
        self.mytool.SetRandUserAgent()
#         myurl = 'http://fangjia.leju.com/shenzhen/longgang/bantian/1009'
        myRoomContent = self.mytool.ReadPageContent(myurl)
        
        myRoomType = self.GetRoomType(myRoomContent)
        
        myBuildingLists = re.findall('<dl class="select_floor clearfix".*?</dl>', myRoomContent, re.S)
        
        try:
            floorIds = re.findall('ShowRoomList\(\'(.*?)\'', myBuildingLists[0], re.S)
            floorNames = re.findall('ShowRoomList\(\'.*?<b>(.*?)<\/b>', myBuildingLists[0], re.S)
        except IndexError:
            return [], []
        
        myRoom = []

        myBlockUrl = re.findall('ShowRoomList\(block[^"]*?\"(.*?)\"', myRoomContent, re.S)[0]
        for myfloor in floorIds:
            myurl = self.rootUrl + myBlockUrl + myfloor
            myRoomListPageContent = self.mytool.ReadPageContent(myurl)
            myRoomLists = re.findall('<span><s></s>(.*?)</span>', myRoomListPageContent, re.S)
            myRoomSizes = re.findall('<cite>(.*?)</cite>', myRoomListPageContent, re.S)
        
            for i in range(len(myRoomLists)):
                myRoomTp = {}
                myfloorName = floorNames[floorIds.index(myfloor)]
                myfloorName = myfloorName.replace('\t', '')
                myfloorName = myfloorName.replace('\r', '')
                myfloorName = myfloorName.replace('\n', '')
                myfloorName = myfloorName.replace(' ', '')
                myRoomTp['floorName'] = myfloorName
                myRoomTp['RoomId'] = self.mytool.EndCharToNoneRex.sub('', myRoomLists[i])
#                 myRoomSize = filter(str.isdigit, str(myRoomSizes[i]))
#                 myRoomTp['RoomSize'] = myRoomSize
                myRoomTp['RoomSize'] = myRoomSizes[i]
                myRoom.append(myRoomTp)
        
        return myRoom, myRoomType
    
    def GetRoomType(self, myPage):
        Types = re.findall('class="room_info clearfix">(.*?)</dl>', myPage, re.S)
        RoomType = []
        for myType in Types:
            try:
                myRoomType = re.findall('(<cite.+class="p_txt">)', myType, re.S)[0]
                myRoomType = self.mytool.EndCharToNoneRex.sub('', myRoomType)
                myRoomType = myRoomType.replace('\t', '')
                myRoomType = myRoomType.replace(' ', '')
                myRoomType = myRoomType.replace('\r', '')
                myRoomType = myRoomType.replace('\n', '')
            except:
                pass
            RoomType.append(myRoomType)
        return RoomType
        
    def WriteDownRooms(self, myRoomSize, myRoomType, myTip, dataPath):
        if not os.path.exists(dataPath):
            os.mkdir(dataPath)
        
        myFile = open(dataPath + '/' + str(myTip['ID']) + '.txt', 'wb')
        myFile.write('Name:%s ID:%d Price:%d\n' % (myTip['Tip'].decode('utf8').encode('gbk'), myTip['ID'], myTip['Price']))
        for RoomType in myRoomType:
            try:
                myFile.write(RoomType + '\n')
            except:
                myFile.write(RoomType.decode('utf8').encode('gbk') + '\n')
            
        for RoomSize in myRoomSize:
#             print chardet.detect(RoomSize['RoomId'])['encoding']
            try:
                myFile.write('%s %s %s\n' % (RoomSize['floorName'], RoomSize['RoomId'].decode('utf8').encode('gbk'), RoomSize['RoomSize']))
            except:
                myFile.write('%s %s %s\n' % (RoomSize['floorName'], RoomSize['RoomId'], RoomSize['RoomSize']))
             
        myFile.close()

if __name__ == '__main__':
    threadNum = 60
    myCityNameList = ['shenzhen']
    
    HousePrice = MyHousePrice.myHousePrices()
    for myCityName in myCityNameList:
        print myCityName
        dataPath = '../PriceData_' + myCityName
        
        myFinishedTip = HousePrice.FinishedTip(dataPath)
        myTips = HousePrice.GetAllTips(myCityName)
        HousePrice.WriteDownIndex(myTips, dataPath)
            
        for tips in myTips:
            while True:
                if str(tips['ID']) in myFinishedTip:
                    break
        #             print threading.enumerate()
                if threading.activeCount() <= threadNum:    
                    HPthread(tips, dataPath).start()
                    break
                else:
                    time.sleep(5)
         
        while True:
            if threading.activeCount() == 1:
                break
            time.sleep(5)