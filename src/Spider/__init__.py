import MyHousePrice
import MyHousePriceThread
import threading
import time

threadNum = 60
myCityNameList = ['shenzhen']

HousePrice = MyHousePrice.myHousePrices()
for myCityName in myCityNameList:
    print myCityName
    dataPath = '../PriceData_' + myCityName
    
    myFinishedTip = HousePrice.FinishedTip(dataPath)
    myTips = HousePrice.GetAllTips(myCityName)
    HousePrice.WriteDownIndex(myTips, dataPath)
        
    for tips in myTips:
        while True:
            if str(tips['ID']) in myFinishedTip:
                break
    #             print threading.enumerate()
            if threading.activeCount() <= threadNum:    
                MyHousePriceThread.HPthread(tips, dataPath).start()
                break
            else:
                time.sleep(5)
     
    while True:
        if threading.activeCount() == 1:
            break
        time.sleep(5)