#!C:\Python27
# -*- coding: utf-8 -*- 

import MyTool
import os
import csv
import re
import math

class CombinePrices():
    def __init__(self):
        self.mytool = MyTool.MyTools()
        
    def CombineFiles(self, dataPath):
        for myCityPath in self.GetCityFilePath(dataPath):
            myCityLists = self.GetCityCommunity(myCityPath)
#             self.WriteToCsv(myCityPath, myCityLists)
        
    def GetCityFilePath(self, dataPath):        
        myCityPath = []
        for listDir in os.listdir(dataPath):
            myDir = listDir.split('_')
            if myDir[0] == 'PriceData':
                myCityPath.append(os.path.join(dataPath, listDir))
        
        return myCityPath
                
    def GetCityCommunity(self, myCityPath):
        myCityLists = []
        
        ff = open(os.path.join(myCityPath, 'indexDetail.txt'), 'rb')
        for myCityList in re.findall('\{.*?\}', ff.read().replace('}\'', '\''), re.S):
            myCity = eval(myCityList)
            myCommunityLists = self.GetFileContent(os.path.join(myCityPath, str(myCity['ID']) + '.txt'), myCity)
            myCityLists.append(myCommunityLists)
            self.WriteToCsvCommunity(myCityPath, myCommunityLists)
            
        ff.close()
        
        return myCityLists
        
    def GetFileContent(self, CommunityPath, myCity):
        ff = open(CommunityPath, 'rb')
        RoomType = {}
        
        lineTag = True
        myCity['RoomName'] = []
        myCity['RoomSize'] = []
        myCity['RoomPrice'] = []
        myCity['RoomPriceAll'] = []
        RoomType['RoomSize'] = []
        RoomType['RoomPrice'] = []
        
        for line in ff.readlines():
            myRoomName = ''
            
            #jump over the first line
            if lineTag:
                lineTag = False
                continue
            
            #Get the room type
            myRoomDetail = line.split(' ')
            for myRoom in line.split(' '):
                if len(myRoom) == 0:
                    myRoomDetail.remove(myRoom)
            
            if len(myRoomDetail) >= 2:
                #get the room Name
                for i in range(2):
                    myRoomName += myRoomDetail[i]
                
                #get the room size
                myRoomSize = filter(str.isdigit, myRoomDetail[len(myRoomDetail) - 1])
                
                if len(myRoomSize) == 0:
                    myRoomSize = 0
                else:
                    myRoomSize = int(myRoomSize)
                
                if len(RoomType['RoomSize']) > 0:
                    myRoomSize2 = []
                    for RoomSize in RoomType['RoomSize']:
                        try:
                            myRoomSize2.append(math.fabs(int(RoomSize) - myRoomSize))
                        except:
                            ff.close()
#                             os.remove(CommunityPath)
                            print myCity['Tip']
                            print RoomSize
                            print RoomType['RoomSize']
                            return myCity
                    myPrice = RoomType['RoomPrice'][myRoomSize2.index(min(myRoomSize2))]

                else:
                    myPrice = myCity['Price']
                
                myPriceAll = int(myPrice)*myRoomSize
                
                myCity['RoomName'].append(myRoomName)
                myCity['RoomSize'].append(str(myRoomSize))
                myCity['RoomPrice'].append(str(myPrice))
                myCity['RoomPriceAll'].append(str(myPriceAll))
                
            else:
                #Get the room type
                [RoomSize, RoomPrice] = self.GetRoomType(line)
                RoomType['RoomSize'].append(RoomSize)
                RoomType['RoomPrice'].append(RoomPrice)
            
        ff.close()
        
        return myCity

    def GetRoomType(self, line):
        line = line.replace(',', '')
        try:
            RoomSize = re.findall('（'.decode('utf8').encode('gbk') + '([\d]*?)' + '㎡）'.decode('utf8').encode('gbk'), line, re.S)[0]
            RoomPrice = re.findall('指导均价：'.decode('utf8').encode('gbk') + '(.*?)' + '元'.decode('utf8').encode('gbk'), line, re.S)[0]
        except IndexError:
            return [], []
        
        return RoomSize, RoomPrice
    
    def WriteToCsv(self, dataPath, myCityLists):
        myKeyNameList = ['ID', 'CommunityName', 'RoomName',  'RoomSize', 'RoomPrice', 'RoomPriceAll']
        
        csvfile = file(os.path.join(dataPath, 'Combine.csv'), 'wb')
        csvwriter = csv.DictWriter(csvfile, fieldnames=myKeyNameList)
        myKeyNameList = dict(zip(myKeyNameList, myKeyNameList))
        myKeyNameList['ID'] = 'id'
        csvwriter.writerow(myKeyNameList)
        
        for myCityList in myCityLists:
              
            myLists = {}
            myLists['ID'] = myCityList['ID']
            myLists['CommunityName'] = myCityList['Tip'].decode('utf8').encode('gbk')
            myLists['RoomName'] = '-'
            myLists['RoomSize'] = '-'
            myLists['RoomPrice'] = str(myCityList['Price'])
            myLists['RoomPriceAll'] = '-'
            csvwriter.writerow(myLists)
              
            for i in range(len(myCityList['RoomName'])):
                  
                myLists = {}
                myLists['ID'] = myCityList['ID']
                myLists['CommunityName'] = myCityList['Tip'].decode('utf8').encode('gbk')
                myLists['RoomName'] = myCityList['RoomName'][i]
                myLists['RoomSize'] = myCityList['RoomSize'][i]
                myLists['RoomPrice'] = myCityList['RoomPrice'][i]
                myLists['RoomPriceAll'] = myCityList['RoomPriceAll'][i]
                csvwriter.writerow(myLists)
                
        csvfile.close()
        
    def WriteToCsvCommunity(self, dataPath, myCommunityLists):
        myKeyNameList = ['ID', 'CommunityName', 'RoomName',  'RoomSize', 'RoomPrice', 'RoomPriceAll']
        
        csvfile = file(os.path.join(dataPath, str(myCommunityLists['ID']) + '.csv'), 'wb')
        csvwriter = csv.DictWriter(csvfile, fieldnames=myKeyNameList)
        myKeyNameList = dict(zip(myKeyNameList, myKeyNameList))
        myKeyNameList['ID'] = 'id'
        csvwriter.writerow(myKeyNameList)
        
        
        for i in range(len(myCommunityLists['RoomName'])):
              
            myLists = {}
            myLists['ID'] = myCommunityLists['ID']
            myLists['CommunityName'] = myCommunityLists['Tip'].decode('utf8').encode('gbk')
            myLists['RoomName'] = myCommunityLists['RoomName'][i]
            myLists['RoomSize'] = myCommunityLists['RoomSize'][i]
            myLists['RoomPrice'] = myCommunityLists['RoomPrice'][i]
            myLists['RoomPriceAll'] = myCommunityLists['RoomPriceAll'][i]
            csvwriter.writerow(myLists)
                
        csvfile.close()
            
if __name__ == '__main__':
    dataPath = '../'
    myCombine= CombinePrices()
    myCombine.CombineFiles(dataPath)